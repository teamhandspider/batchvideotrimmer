﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TrimSelector : MonoBehaviour {

    public Slider timeslider;

    public Slider startSlider;
    public Slider endSlider;

    public VideoPlayer videoPlayer;

    public Text timecodeText;
    public Text limitsText;

    public void PressedOpenVideo() {
        var fill = UnityEditor.EditorUtility.OpenFilePanel("give vid to trim", @"\\MAX-LAPTOP2\Users\OWNER\Videos\Captures", "mp4");
        videoPlayer.url = fill;

        StartCoroutine(SetStartsWhenOpen());
    }

    private IEnumerator SetStartsWhenOpen() {
        while(true) {
            yield return null;

            if (videoPlayer.isPrepared)
                break;

            Debug.Log("loading vid");
            yield return null;
        }

        /*startPoint = 0f;
        endPoint = videoPlayer.clip.length;*/

        var vidLen = videoPlayer.frameCount / videoPlayer.frameRate;

        timeslider.maxValue = vidLen;
        startSlider.maxValue = vidLen;
        endSlider.maxValue = vidLen;

        timeslider.value = 0f;
        startSlider.value = 0f;
        endSlider.value = vidLen;


        UpdateExistingNotifiers();
    }

    public Text existingTrimsNotif;
    private bool autoSettingSlider;

    public Transform loadingIndicator;

    //public Text pendingNotif;
    //public Text doneNotif;

    private void UpdateExistingNotifiers() {

        //pendingNotif.text = "";
        //doneNotif.text = "";

        existingTrimsNotif.text = "";

        var pendings = TrimRunner.GetPendingRequestFiles();
        var doneds = TrimRunner.GetDoneRequestFiles();

        var curr = new System.IO.FileInfo(videoPlayer.url);

        var existingPendings = pendings.Where(x => x.Name.StartsWith(curr.Name));
        var existingDoneds = doneds.Where(x => x.Name.StartsWith(curr.Name));

        if (existingPendings.Count() > 0) {
            foreach (var item in existingPendings) {
                existingTrimsNotif.text += "PENDING:" + item.Name+"\n";
            }
        }

        if (existingDoneds.Count() > 0) {
            foreach (var item in existingDoneds) {
                existingTrimsNotif.text += "DONE:" + item.Name + "\n";
            }
        }
    }

    public void PressedPause() {
        videoPlayer.Pause();
    }

    public void PressedPlay() {
        videoPlayer.Play();
    }

    /*public double startPoint;
    public double endPoint;*/

    /*public void MarkStart() {        
    }

    public void MarkOut() {

    }*/

    private void Update() {
        timecodeText.text = TimeToString(videoPlayer.time);
        limitsText.text = TimeToString(startSlider.value) + "\n" + TimeToString(endSlider.value);

        autoSettingSlider = true;
        timeslider.value = (float)videoPlayer.time;
        autoSettingSlider = false;

        loadingIndicator.gameObject.SetActive(videoPlayer.isPrepared && videoPlayer.clip != null);
        if (!loadingIndicator.gameObject.activeSelf)
            loadingIndicator.transform.rotation = Quaternion.identity;
        else
            loadingIndicator.transform.Rotate(0f, 0f, 360f * Time.deltaTime);
    }

    public void AlteredStartSlider() {
        timeslider.value = startSlider.value;
        videoPlayer.time = timeslider.value;
    }

    public void AlteredEndSlider() {
        timeslider.value = endSlider.value;
        videoPlayer.time = timeslider.value;
    }

    public void AlteredTimeSlider() {
        if(!autoSettingSlider)
            videoPlayer.time = timeslider.value;
    }



    public static string TimeToString(double inTime,bool usePointAsSeparator = false) {

        if(usePointAsSeparator) {
            return System.TimeSpan.FromSeconds(inTime).ToString(@"hh\.mm\.ss");
        }
        else {
            return System.TimeSpan.FromSeconds(inTime).ToString(@"hh\:mm\:ss");
        }

        
    }

    public void CreateTrimmingRequest() {
        var req = new TrimmingRequest();
        req.videoPath = videoPlayer.url;
        req.startPoint = startSlider.value;
        req.endPoint = endSlider.value;

        TrimRunner.InsertNewRequest(req);

        UpdateExistingNotifiers();
    }

    public void DeletePressed() {
        videoPlayer.Stop();
        var vidPath = videoPlayer.url;
        videoPlayer.url = "";

        StartCoroutine(DelVideoSoon(vidPath));
    }

    private IEnumerator DelVideoSoon(string vidPath) {
        yield return new WaitForSecondsRealtime(0.5f);
        Debug.Log("DELETING " + vidPath);
        File.Delete(vidPath);
    }
}

[System.Serializable]
public class TrimmingRequest
{
    public string videoPath;
    public double startPoint;
    public double endPoint;
}
