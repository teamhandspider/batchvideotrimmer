﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;

public class FFmpegShotTest : MonoBehaviour
{
    private static string streamingAssetsPath;

    public static string FFMPEGPath {
        get {
            return streamingAssetsPath + @"/ffmpeg-20190504-a5387f9-win64-static/bin/ffmpeg.exe";
        }
    }
    public static string FFProbePath {
        get {
            return streamingAssetsPath + @"/ffmpeg-20190504-a5387f9-win64-static/bin/ffprobe.exe";
        }
    }

    private void Awake() {
        streamingAssetsPath = Application.streamingAssetsPath;
    }

    public void PressedPick() {
        /*RunCommandGetOutput("ffprobe");
        RunCommandGetOutput("kgdog");
        return;*/

        //RunCommandGetOutput(FFProbePath, "");

        //return;

        float.Parse("150");
        float.Parse("150,86");
        float.Parse("150,867000");


        var fill = UnityEditor.EditorUtility.OpenFilePanel("give vid to trim", @"\\MAX-ATOMICPI-1-\atomtestshare\4TBIfConnected\wellbrowsable", "mp4");
        UnityEngine.Debug.Log("picked " + fill);

        var durout = RunCommandGetOutput(FFProbePath, "-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 " + "\"" + fill + "\"");
        var propNumberString = durout.Split('\r').First().Replace('.',',');
        var secs = float.Parse(propNumberString);
        var durAsSpan = System.TimeSpan.FromSeconds(secs);

        UnityEngine.Debug.Log(durAsSpan.TotalDays + " days long");

    }

    string RunCommandGetOutput(string exePath, string args) {

        UnityEngine.Debug.Log("RunCommandGetOutput " + new FileInfo(exePath).Name + " " + args);

        Process p = new Process();
        // Redirect the output stream of the child process.
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.FileName = exePath;
        p.StartInfo.Arguments = args;
        p.StartInfo.CreateNoWindow = true;

        p.Start();

        var errOutput = p.StandardError.ReadToEnd();
        if(errOutput != "") {
            UnityEngine.Debug.LogError(errOutput);
            return "";
        }
        else {
            var output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            UnityEngine.Debug.Log("out:"+output);
            return output;
        }

    }

    /*string RunCommandGetOutput(string comm) {
        //var process = Process.Start("cmd", "-c " + comm);


        // Start the child process.
        Process p = new Process();
        // Redirect the output stream of the child process.
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.RedirectStandardInput = true;
        p.StartInfo.FileName = "cmd.exe";
        p.StartInfo.Arguments = "/c \"" + comm + "\"";
        p.StartInfo.CreateNoWindow = true;


        p.OutputDataReceived += (x, y) => {
            UnityEngine.Debug.Log("data:" + x);
        };
        p.ErrorDataReceived += (x, y) => {
            UnityEngine.Debug.Log("err:" + x);
        };

        p.Start();

        //p.StandardInput.Write("/c " + comm);

        // Do not wait for the child process to exit before
        // reading to the end of its redirected stream.
        // p.WaitForExit();
        // Read the output stream first and then wait.

        //p.Kill();

        string output = p.StandardOutput.ReadToEnd();
        print("out:"+output);
        p.WaitForExit();
        //p.Kill();

        return output;
    }*/
}
