﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class TrimRunner : MonoBehaviour {

    public static string FFMPEGPath {
        get {
            return streamingAssetsPath + @"/ffmpeg-20190504-a5387f9-win64-static/bin/ffmpeg.exe";
        }
    }

    public static string VideoProcessingsPath {
        get {
            return Application.dataPath /*Application.temporaryCachePath*/ + "/VIDEOPROCESSING~";
        }
    }

    public static string PendingsFolder {
        get {
            return VideoProcessingsPath + "/pending";
        }
    }
    public static string DoneFolder {
        get {
            return VideoProcessingsPath + "/done";
        }
    }
    public static string FailedFolder {
        get {
            return VideoProcessingsPath + "/failed";
        }
    }

    static string streamingAssetsPath;
    static string datapath;

    private void Awake() {
        streamingAssetsPath = Application.streamingAssetsPath;
        datapath = Application.dataPath;

        Application.logMessageReceivedThreaded += Application_logMessageReceivedThreaded;
    }

    string specTag = "";

    private void Application_logMessageReceivedThreaded(string condition, string stackTrace, LogType type) {
        if(specTag == "") {
            specTag = UnityEngine.Random.Range(000000, 999999).ToString();
        }

        LogToFile("[" + System.DateTime.Now + " " + type + "]" + condition + "\n" + stackTrace + "\n",specTag);

    }

    private void LogToFile(string v, string specTag) {
        File.AppendAllText(datapath + "/.trimlog_" + specTag+".txt", v);
    }

    public static void InsertNewRequest(TrimmingRequest req) {
        var filenm = new FileInfo(req.videoPath).Name/*.Substring(0,10)*/;
        filenm += "_trim_" + TrimSelector.TimeToString(req.startPoint,true) + "_TO_" + TrimSelector.TimeToString(req.endPoint,true)+".txt";

        Directory.CreateDirectory(PendingsFolder);
        Directory.CreateDirectory(DoneFolder);

        var json = JsonUtility.ToJson(req,true);
        File.WriteAllText(Path.Combine(PendingsFolder,filenm), json);
    }

    public Toggle runningToggle;
    public bool trimmmingInProgress;

    public GameObject runningPanelRoot;

    private void Update() {

        /*Log("shittt");
        return;*/

        if(runningToggle.isOn && !trimmmingInProgress) {

            runningPanelRoot.SetActive(true);

            FileInfo[] fils = GetPendingRequestFiles();
            var first = fils.Where(k => !k.FullName.EndsWith(".meta")).OrderBy(x => x.CreationTime).FirstOrDefault();
            if (first == null)
                return;

            trimmmingInProgress = true;
            StartCoroutine(TrimmingClip(first));
        }

        foreach (var item in thingsToDoONMainThread) {
            try {
                item();
            }
            catch (Exception e) {
                Debug.LogError("thingsToDoONMainThread crash:"+e);
            }
        }
        thingsToDoONMainThread.Clear();
    }

    List<Action> thingsToDoONMainThread = new List<Action>();

    public static FileInfo[] GetPendingRequestFiles() {
        return new DirectoryInfo(PendingsFolder).GetFiles().Where(x => !x.Name.EndsWith(".meta")).ToArray();
    }
    public static FileInfo[] GetDoneRequestFiles() {
        return new DirectoryInfo(DoneFolder).GetFiles().Where(x => !x.Name.EndsWith(".meta")).ToArray(); ;
    }

    private IEnumerator TrimmingClip(FileInfo requestfile) {
        Log("TrimmingClip " + requestfile.FullName);
        var path = requestfile.FullName;

        if(path.Contains("MAX-LAPTOP") && SystemInfo.deviceName.Contains("MAX-LAPTOP")) {
            Log("Is on maxlaptop and path has MAX-LAPTOP, try convert path! or not");
        }

        var req = JsonUtility.FromJson<TrimmingRequest>(File.ReadAllText(path));


        var sourceVidFileInfo = new FileInfo(req.videoPath);

        if(!sourceVidFileInfo.Exists) {
            Log("SOURCE MISSING! moving to faileds" + req.videoPath);
            Directory.CreateDirectory(FailedFolder);
            var newPath = FailedFolder + "/" + requestfile.Name;
            File.Move(requestfile.FullName, newPath);

            Log("TrimmingClip DONE (failed)");
            trimmmingInProgress = false;
            yield break;
        }

        Log("Input file size:" + ByteLenghtToHumanReadable(sourceVidFileInfo.Length));

        var outputPath = VideoProcessingsPath + "/OUTPUT/" + requestfile.Name + ".mp4";
        Directory.CreateDirectory(VideoProcessingsPath + "/OUTPUT/");
        Log("outputPath:" + outputPath);

        if(File.Exists(outputPath)) {
            Log("TRIMMED VID ALREADY EXISTS:" + outputPath + "deleting it and redoing");
            File.Delete(outputPath);            
        }

        bool don = false;
        bool success = false;
        var ffmpegrunnerThread = new Thread(() => {
            try {
                success = RunffmpegCommandForTrim(req, FFMPEGPath, outputPath);
            }
            catch(System.Exception e) {
                Log("FFMPEG CRASH:" + e,true);
                success = false;
                enabled = false;
            }
            don = true;
        });
        ffmpegrunnerThread.Start();

        while(true) {
            yield return null;

            if (don)
                break;
        }
        var fil = new FileInfo(outputPath);
        if(!fil.Exists) {
            Log("NO FILE OUTPUTTED! FFmpeg failed", true);
            success = false;
        }
        else if(fil.Length < 10000) {
            Log("VERY SMALL FILE OUTPUTTED! FFmpeg failed", true);
            success = false;
        }

        if(fil.Exists) {
            Log("Output file size:" + ByteLenghtToHumanReadable(fil.Length));
        }

        if(success) {
            var newPath = DoneFolder + "/" + requestfile.Name;
            if (File.Exists(newPath)) {
                Log("in donefolder: already exists. wtf, probably just BS, delling");
                File.Delete(newPath);
                Thread.Sleep(1000);
            }

            Log("Moving reqfile to doneds: " + newPath);
            File.Move(requestfile.FullName, newPath);

            //predelete
            Log("Pre-deleting source vid");
            var predelfolderpath = @"C:\PREDELETE_TRIMMEDVIDEOS";
            Directory.CreateDirectory(predelfolderpath);
            File.Move(sourceVidFileInfo.FullName, predelfolderpath+"/"+sourceVidFileInfo.Name);
        }


        Log("TrimmingClip DONE");
        trimmmingInProgress = false;
    }

    private void Log(string v, bool error = false) {
        v = "[" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture) + /*" " + appTag + " " + */System.Environment.MachineName + "]" + v;
        if (error)
            v = "ERROR:" + v;

        if (error)
            Debug.LogError("TrimRunner " + v);
        else
            Debug.Log("TrimRunner " + v);

        thingsToDoONMainThread.Add(() => {
            logText.text = logText.text + "\n" + v;
        });
    }

    public Text logText;

    /*public void StartRunning() {
        //StartCoroutine(RunningTrimmings());
    }*/

    /*private IEnumerator RunningTrimmings() {
        Debug.Log("RunningTrimmings");
    }*/



    private bool RunffmpegCommandForTrim(TrimmingRequest req, string FFMPEGPath, string outputPath) {
        var ffmpegProcess = new System.Diagnostics.Process();
        ffmpegProcess.StartInfo.FileName = FFMPEGPath;
        //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        ffmpegProcess.StartInfo.CreateNoWindow = true;
        ffmpegProcess.StartInfo.WorkingDirectory = new FileInfo(req.videoPath).DirectoryName;
        print("workingdir:" + ffmpegProcess.StartInfo.WorkingDirectory);

        ffmpegProcess.StartInfo.RedirectStandardOutput = true;
        ffmpegProcess.StartInfo.RedirectStandardError = true;
        ffmpegProcess.StartInfo.UseShellExecute = false;

        var lastStdErrOutPuts = new StringBuilder();

        ffmpegProcess.OutputDataReceived += Process_OutputDataReceived;
        ffmpegProcess.ErrorDataReceived += Process_ErrorDataReceived;

        //var arguments = "-framerate " + Mathf.RoundToInt(targetFPS) + " -i frame%010d" + ImageFileExtension + " -loglevel " + ffmpegLogLevelString + " -crf 25 \"" + saveToPath + "\"";

        var durationsecs = req.endPoint - req.startPoint;
        var durationSTR = TrimSelector.TimeToString(durationsecs);

        var arguments = "-i \"" + req.videoPath + "\" -ss " + TrimSelector.TimeToString(req.startPoint) + " -t " + durationSTR + " -codec copy \""+outputPath+"\"";

        print("arguments: " + arguments);
        ffmpegProcess.StartInfo.Arguments = arguments;

        //lastReceiveFromErrorTube = "";

        //lastInfoWithMaybeProgress = "";
        //ffmpegFramesProcessed = 0;

        ffmpegProcess.Start();
        ffmpegProcess.BeginErrorReadLine();
        ffmpegProcess.BeginOutputReadLine();

        do {
            Thread.Sleep(10);
        } while (!ffmpegProcess.HasExited);


        return true;
    }

    public List<string> fromFFmpegProcess = new List<string>();

    private void Process_ErrorDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e) {
        var str = "Process_ErrorDataReceived " + e.Data;
        fromFFmpegProcess.Add(str);
        Debug.Log(str);
    }

    private void Process_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e) {
        var str = "Process_OutputDataReceived " + e.Data;
        fromFFmpegProcess.Add(str);
        Debug.Log(str);
    }


    public static string ByteLenghtToHumanReadable(long byteLenght, bool useBits = false) {
        string suffix = "";
        long lenght;
        if (useBits) lenght = byteLenght * 8;
        else lenght = byteLenght;

        if (lenght < 1024) {
            if (useBits) suffix = " bits";
            else suffix = " B";
            return lenght + suffix;
        }
        else if (lenght < 1048576) {
            if (useBits) suffix = " kbits";
            else suffix = " kB";
            return (lenght / 1024) + suffix;
        }
        else {
            if (useBits) suffix = " Mbits";
            else suffix = " MB";
            return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
        }
    }
}
