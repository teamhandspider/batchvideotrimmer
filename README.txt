DISCLAIMER: I created this system only for my own use so UX might be subpar.

batchvideotrimmer allows easily clipping irrelevant parts from the start and end of video files, and losslessly saving them to files to be processed later.
My own use case required quickly marking in/out points for many videos and processing the videos on another machine from the one where the cut in / out points are selected,
which is automatically supported if the videos reside in a SMB-like path.


batchvideotrimmer utilizes Unity's own VideoPlayer component for video preview and ffmpeg for the actual video processing.